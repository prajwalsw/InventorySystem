/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.inventorysystem.controller;

import com.leapfrog.inventorysystem.dao.ProductDAO;
import com.leapfrog.inventorysystem.dao.SupplierDAO;
import com.leapfrog.inventorysystem.daoimpl.ProductDAOImpl;
import com.leapfrog.inventorysystem.daoimpl.SupplierDAOImpl;
import com.leapfrog.inventorysystem.entity.Suppliers;
import com.leapfrog.inventorysystem.entity.Products;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Boolean.TRUE;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Prajwal
 */
@WebServlet(name="default", urlPatterns ={"/default"})
public class DefaultController extends HttpServlet {

    //private SupplierDAO supDAO = new SupplierDAOImpl();
    private ProductDAO proDAO = new ProductDAOImpl();

    /*@Overridefor inserting supplier
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       try{ int result=supDAO.insert(new Suppliers(0,"XYZ Manufacturers", "xyz@email.com", 983985935));
        response.getWriter().println(result+" "+ "supplier has been added");
        }catch(ClassNotFoundException|SQLException se){
            System.out.println(se.getMessage());
        }
    }
     */
    /*@Override Getting Suppliers
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            PrintWriter out = response.getWriter();
            for (Suppliers supplier : supDAO.getAll()) {
                out.print("<h1>");
                out.println(supplier.getName() + " " + supplier.getEmail() + " " + supplier.getContactNo());
                out.print("</h1>");

            }
        } catch (ClassNotFoundException | SQLException se) {
            System.out.println(se.getMessage());
        }
    }*/
  /*  @Override for Inserting Products
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       try{ 
        
         Products product=new Products();
         product.setId(0);
         product.setName("Hat");
         product.setDescription("Nepali Hat");
         product.setQuantity(10);
         product.setPrice(250);
         product.setMargin(50);
         product.setStatus(true);
         product.setSupplier(new Suppliers(1));
         int result=proDAO.insert(product);
           
        //response.getWriter().println(result+" "+ "product has been added");
        PrintWriter out=response.getWriter();
        out.println(result+" "+ "product has been added");
        out.println(product.getName()+" "+"from"+" "+product.getSupplier().getName());
        }catch(ClassNotFoundException|SQLException se){
            System.out.println(se.getMessage());
        }
    }
*/
    @Override 
            //Getting Products
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            PrintWriter out = response.getWriter();
            for ( Products product : proDAO.getAll()) {
                out.print("<h1>");
                out.println(product.getName() + " " + product.getSupplier().getName() + " " + product.isStatus());
                out.print("</h1>");

            }
        } catch (ClassNotFoundException | SQLException se) {
            System.out.println(se.getMessage());
        }
    }
}
