/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.inventorysystem.controller;

import com.leapfrog.inventorysystem.dao.ProductDAO;
import com.leapfrog.inventorysystem.daoimpl.ProductDAOImpl;
import com.leapfrog.inventorysystem.entity.Products;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Prajwal
 */
@WebServlet(name = "product", urlPatterns = {"/product/*"})
public class ProductController extends HttpServlet {

    private ProductDAO proDAO = new ProductDAOImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setAttribute("product", proDAO.getAll());
            request.getRequestDispatcher("/WEB-INF/products/index.jsp").forward(request, response);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

}
