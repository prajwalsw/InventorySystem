/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.inventorysystem.controller;

import com.leapfrog.inventorysystem.dao.SupplierDAO;
import com.leapfrog.inventorysystem.daoimpl.SupplierDAOImpl;
import com.leapfrog.inventorysystem.entity.Suppliers;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Prajwal
 */
@WebServlet(name = "supplier", urlPatterns = {"/supplier/*"})
public class SupplierController extends HttpServlet {

    private SupplierDAO supDAO = new SupplierDAOImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String uri=request.getRequestURI().toLowerCase();
        if(uri.contains("/add")){
        request.getRequestDispatcher("/WEB-INF/suppliers/index.jsp").forward(request, response);
        }
        else{
            request.getRequestDispatcher("/WEB-INF/suppliers/supplier.jsp").forward(request, response);
            // response.sendRedirect(request.getContextPath()+"/supplier");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            Suppliers supplier = new Suppliers();
            supplier.setName(request.getParameter("supplier_name"));

            supplier.setEmail(request.getParameter("supplier_email"));

            supplier.setContactNo(Integer.parseInt(request.getParameter("supplier_contactno")));
            int result = supDAO.insert(supplier);
//            response.getWriter().println(supplier.getName() + " " + "supplier has been added");
            //to show all suppliers
            //request.setAttribute("supplier", supDAO.getAll());
            //request.getRequestDispatcher("/WEB-INF/suppliers/supplier.jsp").forward(request, response);
            response.sendRedirect(request.getContextPath()+"/supplier");

        } catch (Exception ex) {
            System.out.println(ex.getMessage());

        }
    }

}
