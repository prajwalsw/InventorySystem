/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jason.inventorysystem.controller;

import com.leapfrog.inventorysystem.dao.ProductDAO;
import com.leapfrog.inventorysystem.dao.PurchaseOrderDAO;
import com.leapfrog.inventorysystem.daoimpl.ProductDAOImpl;
import com.leapfrog.inventorysystem.daoimpl.PurchaseOrderDAOImpl;
import com.leapfrog.inventorysystem.entity.Products;
import com.leapfrog.inventorysystem.entity.PurchaseOrder;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Prajwal
 */
@WebServlet(name = "order", urlPatterns = {"/order/*"})

public class purchaseOrderController extends HttpServlet {

    private PurchaseOrderDAO order = new PurchaseOrderDAOImpl();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String uri = request.getRequestURI().toLowerCase();
            if (uri.contains("/add")) {
                ProductDAO product = new ProductDAOImpl();
                request.setAttribute("proDAO", product.getAll());
                request.getRequestDispatcher("/WEB-INF/purchase_order/addOrder.jsp").forward(request, response);
            } else {
                request.setAttribute("orderDAO", order.getAll());

                request.getRequestDispatcher("/WEB-INF/purchase_order/order.jsp").forward(request, response);
                response.getWriter().println("<h1> COMING SOON</h1>");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            
        
            PurchaseOrder purOrder = new PurchaseOrder();
              System.out.println("AB");
             
              System.out.println("ABagjagkja");
              System.out.println(request.getParameter("order_quantity"));
              System.out.println(request.getParameter("expected_date"));
              System.out.println(request.getParameter("product_id"));
            purOrder.setProduct(new Products(Integer.parseInt(request.getParameter("product_id"))));
            System.out.println("ABc");

            purOrder.setQuantity(Integer.parseInt(request.getParameter("order_quantity")));
            //date ko lagi
              System.out.println("ABcd"); 
              
            String testDate = request.getParameter("expected_date");
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(testDate);
           // System.out.println(date);
          
             // java.sql.Date mySqlDate = new java.sql.Date(date);

            purOrder.setExpectedDate(date);
            System.out.println("ABcdeeeee");
            int result = order.insert(purOrder);
//            response.getWriter().println(order.getName() + " " + "order has been added");
            //to show all orders
            //request.setAttribute("order", supDAO.getAll());
            //request.getRequestDispatcher("/WEB-INF/orders/order.jsp").forward(request, response);
            response.sendRedirect(request.getContextPath() + "/order");

        } catch (Exception ex) {
            System.out.println(ex.getMessage());

        }
    }

}
