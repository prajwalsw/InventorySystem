/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.inventorysystem.constant;

/**
 *
 * @author Prajwal
 */
public class SQLConstant {

    public final static String SUPPLIER_INSERT = "INSERT INTO suppliers(supplier_name,supplier_email,supplier_contactno) VALUES(?,?,?)";
    public final static String SUPPLIER_GETALL = "SELECT * FROM suppliers";

    public final static String PRODUCT_INSERT = "INSERT INTO products(product_name,product_description,product_quantity,product_price,product_margin,product_status,supplier_id) VALUES(?,?,?,?,?,?,?)";
    public final static String PRODUCT_GETALL = "SELECT * FROM products pro JOIN suppliers sup on pro.supplier_id=sup.supplier_id";

    public final static String ORDER_INSERT = "INSERT INTO purchase_orders(product_id,order_quantity,expected_date) VALUES(?,?,?)";
    public final static String ORDER_GETALL = "SELECT * FROM purchase_orders pord  JOIN products pro on pro.product_id=pord.product_id JOIN Suppliers sup on sup.supplier_id=pro.supplier_id";

}
