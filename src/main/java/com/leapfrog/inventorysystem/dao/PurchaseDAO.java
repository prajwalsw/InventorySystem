/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.inventorysystem.dao;

import com.leapfrog.inventorysystem.entity.Purchase;
import java.sql.SQLException;
import java.util.List;


/**
 *
 * @author Prajwal
 */
public interface PurchaseDAO {
    List<Purchase> getAll()throws ClassNotFoundException, SQLException ;
    int insert(Purchase purchase)throws ClassNotFoundException, SQLException ;
}
