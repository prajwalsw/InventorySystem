/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.inventorysystem.dao;

import com.leapfrog.inventorysystem.entity.PurchaseOrder;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Prajwal
 */
public interface PurchaseOrderDAO {
    List<PurchaseOrder> getAll()throws ClassNotFoundException, SQLException;
    int insert(PurchaseOrder order)throws ClassNotFoundException, SQLException;
}
