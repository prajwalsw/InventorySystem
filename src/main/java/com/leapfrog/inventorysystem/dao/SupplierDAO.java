/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.inventorysystem.dao;

import java.util.List;
import com.leapfrog.inventorysystem.entity.Suppliers;
import java.sql.SQLException;

/**
 *
 * @author Prajwal
 */
public interface SupplierDAO {
    List<Suppliers> getAll()throws ClassNotFoundException,SQLException;
    int insert(Suppliers supplier)throws ClassNotFoundException,SQLException;
}
