/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.inventorysystem.daoimpl;

import com.leapfrog.inventorysystem.constant.SQLConstant;
import com.leapfrog.inventorysystem.dao.ProductDAO;
import com.leapfrog.inventorysystem.dbutil.DbConnection;
import com.leapfrog.inventorysystem.entity.Products;
import com.leapfrog.inventorysystem.entity.Suppliers;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Prajwal
 */
//insert--> webpage to object and to database
//getall-->> database to object and to webpage
public class ProductDAOImpl implements ProductDAO {

    private DbConnection db = new DbConnection();

    @Override
    //database bta taaneko data an object maa mapping
    public List<Products> getAll() throws ClassNotFoundException, SQLException {
        List<Products> products = new ArrayList<>();
        db.connect();
        db.initStatement(SQLConstant.PRODUCT_GETALL);
        ResultSet rs = db.query();
        while (rs.next()) {
            Products product = new Products();
            product.setId(rs.getInt("product_id"));
            product.setName((rs.getString("product_name")));
            product.setDescription(rs.getString("product_description"));
            product.setQuantity(rs.getInt("product_quantity"));
            product.setPrice(rs.getInt("product_price"));
            product.setMargin(rs.getInt("product_margin"));
            product.setStatus(rs.getBoolean("product_status"));
            Suppliers supplier=new Suppliers(rs.getInt("supplier_id"), rs.getString("supplier_name"), rs.getString("supplier_email"), rs.getInt("supplier_contactno"));
            product.setSupplier(supplier);
            products.add(product);

        }
        db.close();
        return products;

    }

    @Override
    //database maa insert vaneko ho..paila..list maa object add gareyjastai
    public int insert(Products product) throws ClassNotFoundException, SQLException {
        db.connect();
        PreparedStatement stmt = db.initStatement(SQLConstant.PRODUCT_INSERT);
        stmt.setString(1, product.getName());
        stmt.setString(2, product.getDescription());
        stmt.setInt(3, product.getQuantity());
        stmt.setInt(4, product.getPrice());
        stmt.setInt(5, product.getMargin());
        stmt.setBoolean(6, product.isStatus());
        stmt.setInt(7,product.getSupplier().getId());
        int result = db.update();
        db.close();
        return result;
    }

}
