/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.inventorysystem.daoimpl;

import com.leapfrog.inventorysystem.constant.SQLConstant;
import com.leapfrog.inventorysystem.dao.PurchaseOrderDAO;
import com.leapfrog.inventorysystem.dbutil.DbConnection;
import com.leapfrog.inventorysystem.entity.Products;
import com.leapfrog.inventorysystem.entity.PurchaseOrder;
import com.leapfrog.inventorysystem.entity.Suppliers;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Prajwal
 */
//insert--> webpage to object and to database
//getall-->> database to object and to webpage
public class PurchaseOrderDAOImpl implements PurchaseOrderDAO {

    private DbConnection db = new DbConnection();

    @Override
    //database bta taaneko data an object maa mapping
    public List<PurchaseOrder> getAll() throws ClassNotFoundException, SQLException {
        List<PurchaseOrder> purchaseOrders = new ArrayList<>();
        db.connect();
        db.initStatement(SQLConstant.ORDER_GETALL);
        ResultSet rs = db.query();
        while (rs.next()) {
            PurchaseOrder purchaseOrder = new PurchaseOrder();
            purchaseOrder.setId(rs.getInt("order_id"));
            Products product;
            // int supId=rs.getInt("supplier_id");
            Suppliers supplier = new Suppliers(rs.getInt("supplier_id"), rs.getString("supplier_name"), rs.getString("supplier_email"), rs.getInt("supplier_contactno"));
            product = new Products(rs.getInt("product_id"), rs.getString("product_name"), rs.getString("product_description"), rs.getInt("product_quantity"), rs.getInt("product_price"), rs.getInt("product_margin"), rs.getBoolean("product_status"), supplier);

            purchaseOrder.setProduct(product);
            purchaseOrder.setQuantity(rs.getInt("order_quantity"));
            purchaseOrder.setOrderDate(rs.getDate("order_date"));
            purchaseOrder.setExpectedDate(rs.getDate("expected_date"));
            purchaseOrders.add(purchaseOrder);
            System.out.println(product.getName());
            System.out.println(rs.getDate("order_date"));

        }
        db.close();
        return purchaseOrders;

    }

    @Override
    //database maa insert vaneko ho..paila..list maa object add gareyjastai
    public int insert(PurchaseOrder purchaseOrder) throws ClassNotFoundException, SQLException {
        db.connect();
        PreparedStatement stmt = db.initStatement(SQLConstant.ORDER_INSERT);

        stmt.setInt(1, purchaseOrder.getProduct().getId());
        stmt.setInt(2, purchaseOrder.getQuantity());
        stmt.setDate(3, new java.sql.Date(purchaseOrder.getExpectedDate().getTime()));
        int result = db.update();
        db.close();
        return result;
    }

}
