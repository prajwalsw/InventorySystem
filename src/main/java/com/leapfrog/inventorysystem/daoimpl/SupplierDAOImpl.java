/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.inventorysystem.daoimpl;

import com.leapfrog.inventorysystem.constant.SQLConstant;
import com.leapfrog.inventorysystem.dao.SupplierDAO;
import com.leapfrog.inventorysystem.dbutil.DbConnection;
import com.leapfrog.inventorysystem.entity.Suppliers;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Prajwal
 */
//insert--> webpage to object and to database
//getall-->> database to object and to webpage
public class SupplierDAOImpl implements SupplierDAO {

    private DbConnection db = new DbConnection();

    @Override
    //database bta taaneko data an object maa mapping
    public List<Suppliers> getAll() throws ClassNotFoundException, SQLException {
        List<Suppliers> suppliers = new ArrayList<>();
        db.connect();
        db.initStatement(SQLConstant.SUPPLIER_GETALL);
        ResultSet rs = db.query();
        while (rs.next()) {
            Suppliers supplier = new Suppliers();
            supplier.setId(rs.getInt("supplier_id"));
            supplier.setName((rs.getString("supplier_name")));
            supplier.setEmail(rs.getString("supplier_email"));
            supplier.setContactNo(rs.getInt("supplier_contactno"));
            suppliers.add(supplier);

        }
        db.close();
        return suppliers;

    }

    @Override
    //database maa insert vaneko ho..paila..list maa object add gareyjastai
    public int insert(Suppliers supplier) throws ClassNotFoundException, SQLException {
        db.connect();
        PreparedStatement stmt = db.initStatement(SQLConstant.SUPPLIER_INSERT);
        stmt.setString(1, supplier.getName());
        stmt.setString(2, supplier.getEmail());
        stmt.setInt(3, supplier.getContactNo());
        int result = db.update();
        db.close();
        return result;
    }

}
