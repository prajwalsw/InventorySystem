/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leapfrog.inventorysystem.entity;

import java.util.Date;

/**
 *
 * @author Prajwal
 */
public class PurchaseOrder {

    private int id;
    private Products product;
    private int quantity;

    private Date orderDate;
    private Date expectedDate;

    public PurchaseOrder() {
    }

    public PurchaseOrder(int id, Products product, int quantity, Date expectedDate) {
        this.id = id;
        this.product = product;
        this.quantity = quantity;

        this.expectedDate = expectedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getExpectedDate() {
        return expectedDate;
    }

    public void setExpectedDate(Date expectedDate) {
        this.expectedDate = expectedDate;
    }

}
