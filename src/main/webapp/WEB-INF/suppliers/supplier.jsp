<%-- 
    Document   : supplier
    Created on : Jul 1, 2017, 11:21:57 AM
    Author     : Prajwal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="supp" class="com.leapfrog.inventorysystem.daoimpl.SupplierDAOImpl"/> 
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="BASE_URL" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



    </head>
    <body>
        <div class="container">
            <h1>List of Suppliers</h1>
            <div class="pull-right">
                <p>
                    <a href="${BASE_URL}/supplier/add" class="btn btn-primary">
                        Add Supplier
                    </a>
                </p>
            </div>
            <table class="table table-bordered">
                <tr>
                    <th>S.N</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact Number</th>


                </tr>
                <c:forEach var="supplier" items="${supp.all}">
                    <tr>
                        <td>${supplier.id}</td>
                        <td>${supplier.name}</td>
                        <td>${supplier.email}</td>
                        <td>${supplier.contactNo}</td>
                    </tr>
                </c:forEach>
            </table>
            <a href="${BASE_URL}/home" class="btn btn-danger">Back</a>
        </div>
    </body>
</html>
