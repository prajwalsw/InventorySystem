<%-- 
    Document   : home
    Created on : Jul 1, 2017, 10:11:08 PM
    Author     : Prajwal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="BASE_URL" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>




    </head>
    <body>
        <div class="container" style="background-image: url('/WEB-INF/home/picture.jpg');">
            
            <div class="row">
                <div class="col-md-6 col-md-offset-3">                   
                    <a href="${BASE_URL}/supplier">
                        <button type="button" class="btn btn-primary btn-lg btn-block">Suppliers</button>
                    </a>

                    <a href="${BASE_URL}/product">
                        <button type="button" class="btn btn-default btn-lg btn-block">Products</button>
                    </a>
                    <a href="${BASE_URL}/order">
                        <button type="button" class="btn btn-primary btn-lg btn-block">Order</button>
                    </a>
                    <button type="button" class="btn btn-default btn-lg btn-block">Purchase List</button>
                </div>
            </div>
        </div>
    </body>
</html>
