<%-- 
    Document   : order
    Created on : Jul 2, 2017, 10:44:55 AM
    Author     : Prajwal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:set var="BASE_URL" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>




    </head>
    <body>
        <div class="container">
            <h1>List of Orders</h1>
            <div class="pull-right">
                <p>
                    <a href="${BASE_URL}/order/add" class="btn btn-primary">
                        Add Order
                    </a>
                </p>
            </div>
            <table class="table table-bordered">
                <tr>
                    <th>S.N</th>
                    <th>Product Id</th>
                    <th>Product Name</th>
                    <th>Order Quantity</th>
                    <th>To Supplier Id</th>
                    <th>To Supplier</th>
                    <th>Order Date</th>
                    <th>Expected Date</th>
                </tr> 

                <c:forEach var="order" items="${requestScope.orderDAO}">
                    <tr>
                        <td>${order.id}</td>
                        <td>${order.product.id}</td>
                        <td>${order.product.name}</td>
                        <td>${order.quantity}</td>
                        <td>${order.product.supplier.id}</td>
                        <td>${order.product.supplier.name}</td>
                        <td>${order.orderDate}</td>
                        <td>${order.expectedDate}</td>
                    </tr>
                </c:forEach>

            </table>
            <a href="${BASE_URL}/home" class="btn btn-danger">Back</a>
        </div>
    </body>
</html>
